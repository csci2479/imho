const path = require('path');
const fs = require('fs');
const jsPath = path.resolve(__dirname, 'assets/src/js/admin');
const entries = {};

// get entries
(() => {
    try {
        const files = fs.readdirSync(jsPath);
        files.forEach(f => {
            const filePath = path.resolve(jsPath, f);
            entries[f.replace(/\.js$/, '')] = filePath
        });
    } catch (err) {
        console.error(err);
    }
})();

module.exports = {
    entry: entries,
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'assets/dist/js')
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    }
};
