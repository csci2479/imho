<?php
namespace Imho\Repos\Interfaces;

interface IGameRepo {
  public function getGames() : array;
  public function deleteGame(int $id) : bool;
  public function addGame(string $title, int $sysId, int $year, bool $completed) : int;
  public function editGame(int $id, string $title, int $sysId, int $year, bool $completed): bool;
}
