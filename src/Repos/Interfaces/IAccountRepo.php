<?php
namespace Imho\Repos\Interfaces;

interface IAccountRepo
{
    public function createNewUser(string $username, string $password) : bool;
    public function getUserRecord(string $username) : array;
}
