<?php
namespace Imho\Repos\Implementations;

use \Imho\Repos\Interfaces\IGameRepo as IGameRepo;

final class GameRepo implements IGameRepo
{

    /**
    * @Inject("logger")
    */
    private $logger;

    /**
    * @Inject("dbConnection")
    */
    private $conn;

    public function getGames() : array
    {
        return $this->conn->fetchAll('SELECT g.id as game_id, g.title as game_title,
            g.release_year, g.completed, s.id, s.title FROM game g
            join system s on g.system_id = s.id ORDER BY title');
    }

    public function deleteGame(int $id) : bool
    {
        try {
            $sql = 'DELETE FROM game WHERE id = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $id);

            $success = $stmt->execute();

            if (!$success) {
                throw new \Exception($stmt->error);
            }

            return true;
        } catch (\Throwable $e) {
            $this->logger->info($e->getMessage());
            throw $e;
        }
    }

    public function addGame(string $title, int $sysId, int $year, bool $completed) : int
    {
        try {
            $sql = 'INSERT INTO game (title, system_id, release_year, completed)
                VALUES(?, ?, ?, ?)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $title);
            $stmt->bindValue(2, $sysId);
            $stmt->bindValue(3, (new \DateTime($year))->format('Y'));
            $stmt->bindValue(4, $completed, 'boolean');

            $success = $stmt->execute();

            if (!$success) {
                throw new \Exception($stmt->error);
            }

            return $this->conn->lastInsertId();
        } catch (\Throwable $e) {
            $this->logger->info($e->getMessage());
            throw $e;
        }
    }

    public function editGame(int $id, string $title, int $sysId, int $year, bool $completed): bool
    {
        try {
            $sql = 'UPDATE game set title = ?, system_id = ?, release_year = ?,
                completed = ? where id = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $title);
            $stmt->bindValue(2, $sysId);
            $stmt->bindValue(3, (new \DateTime($year))->format('Y'));
            $stmt->bindValue(4, $completed, 'boolean');
            $stmt->bindValue(5, $id);

            $success = $stmt->execute();

            if (!$success) {
                throw new \Exception($stmt->error);
            }

            return true;
        } catch (\Throwable $e) {
            $this->logger->info($e->getMessage());
            throw $e;
        }
    }
}
