<?php
namespace Imho\Repos\Implementations;

use \Imho\Repos\Interfaces\IBlogRepo as IBlogRepo;

final class BlogRepo implements IBlogRepo
{
    /**
     * @Inject("logger")
     */
    private $logger;

    /**
     * @Inject("dbConnection")
     */
    private $conn;

    public function getBlogs() : array
    {
        return $this->conn->fetchAll('SELECT b.*, g.title as game_title,
            g.id as game_id, g.release_year, g.completed, s.id as system_id,
            s.title as system_title
            FROM blog b
            JOIN game g ON b.game_id = g.id
            JOIN system s on s.id = g.system_id
            ORDER BY b.date_created DESC');
    }

    public function addBlog(string $title, string $body, int $gameId, int $userId) : int
    {
        try {
            $sql = 'INSERT INTO blog(title, body, game_id, user_id)
                VALUES(?, ?, ?, ?)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $title);
            $stmt->bindValue(2, $body);
            $stmt->bindValue(3, $gameId);
            $stmt->bindValue(4, $userId);

            $success = $stmt->execute();

            if (!$success) {
                throw new \Exception($stmt->error);
            }

            return $this->conn->lastInsertId();
        } catch (\Throwable $e) {
            $this->logger->info($e->getMessage());
            throw $e;
        }
    }

    public function editBlog(int $id, string $title, string $body, int $gameId, int $userId) : bool
    {
        try {
            $sql = 'UPDATE blog SET title = ?, body = ?, game_id = ?, user_id = ?
                WHERE id = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $title);
            $stmt->bindValue(2, $body);
            $stmt->bindValue(3, $gameId);
            $stmt->bindValue(4, $userId);
            $stmt->bindValue(5, $id);

            $success = $stmt->execute();

            if (!$success) {
                throw new \Exception($stmt->error);
            }

            return true;
        } catch (\Throwable $e) {
            $This->logger->info($e->getMessage());
            throw $e;
        }
    }

    public function deleteBlog(int $id) : bool
    {
        try {
            $sql = 'DELETE FROM blog WHERE id = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $id);

            $success = $stmt->execute();

            if (!$success) {
                throw new \Exception($stmt->error);
            }

            return true;
        } catch (\Throwable $e) {
            $this->logger->info($e->getMessage());
            throw $e;
        }
    }
}
