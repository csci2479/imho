<?php
namespace Imho\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Slim\Views\Twig;
use \Imho\Services\Interfaces\ISystemService;

final class HomeController
{
    private $view;
    private $systemService;

    /**
     * @Inject("session")
     */
    private $session;

    /**
     * @Inject("cache")
     */
    private $cache;

    public function __construct(Twig $view, ISystemService $systemService)
    {
        $this->view = $view;
        $this->systemService = $systemService;
    }

    public function home(Request $request, Response $response)
    {
        return $this->view->render($response, 'home.html.twig', [
            'user' => $this->session->user,
            'keys' => $this->cache->getAllKeys(),
        ]);
    }
}
