<?php
namespace Imho\Services\Interfaces;

interface ISystemService
{
    public function getSystems() : array;
    public function deleteSystem(int $id) : bool;
    public function addSystem(string $name) : int;
    public function editSystem(int $id, string $name) : bool;
}
