<?php
namespace Imho\Services\Implementations;

use \Imho\Services\Interfaces\ISystemService;
use \Imho\Repos\Interfaces\ISystemRepo;
use \Imho\Models\System;

final class SystemService implements ISystemService
{
    /**
     * @Inject("logger")
     */
    private $logger;

    /**
     * @Inject("cache")
     */
    private $cache;

    protected $systemRepo;

    const KEY = 'GET_SYSTEMS';

    public function __construct(ISystemRepo $systemRepo)
    {
        $this->systemRepo = $systemRepo;
    }

    public function getSystems() : array
    {
        $mapSystemModel = function ($row) {
            return new System($row['id'], $row['title']);
        };

        // check for item in cache
        $systems = $this->cache->get(self::KEY);

        if (!$systems) {
            // not in cache, so make db call and put in cache
            $systems = $this->systemRepo->getSystems();
            $this->cache->add(self::KEY, $systems);
        }

        return array_map($mapSystemModel, $systems);
    }

    public function deleteSystem(int $id) : bool
    {
        try {
            $success = $this->systemRepo->deleteSystem($id);

            if ($success) {
                $this->cache->delete(self::KEY); // clear cache
            }

            return $success;
        } catch (\Throwable $e) {
            $this->logger->notice('Exception when calling ' . __METHOD__);

            return false;
        }
    }

    public function addSystem(string $name) : int
    {
        try {
            $id = $this->systemRepo->addSystem($name);

            if ($id) {
                $this->cache->delete(self::KEY); // clear cache
            }

            return $id;
        } catch (\Throwable $e) {
            $this->logger->notice('Exception when calling ' . __METHOD__);

            return 0;
        }
    }

    public function editSystem(int $id, string $name) : bool
    {
        try {
            $success = $this->systemRepo->editSystem($id, $name);

            if ($success) {
                $this->cache->delete(self::KEY);
            }

            return $success;
        } catch (\Throwable $e) {
            $this->logger->notice('Exception when calling ' . __METHOD__);

            return false;
        }
    }
}
