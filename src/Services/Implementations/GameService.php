<?php
namespace Imho\Services\Implementations;

use \Imho\Services\Interfaces\IGameService;
use \Imho\Repos\Interfaces\IGameRepo;
use \Imho\Models\Game;
use \Imho\Models\System;

final class GameService implements IGameService
{
    /**
     * @Inject("logger")
     */
    private $logger;

    /**
     * @Inject("cache")
     */
    private $cache;

    protected $gameRepo;

    const KEY = 'GET_GAMES';

    public function __construct(IGameRepo $gameRepo)
    {
        $this->gameRepo = $gameRepo;
    }

    public function getGames() : array
    {
        $mapGameModel = function ($row) {
            $system = new System($row['id'], $row['title']);

            return new Game($row['game_id'], $row['game_title'], $system,
                $row['release_year'], $row['completed']);
        };

        $games = $this->getGamesFromCache();

        return array_map($mapGameModel, $games);
    }

    public function getGame(int $id) : Game
    {
        $games = $this->getGames();

        foreach($games as $game) {
            if ($id === $game->id) {
                return $game;
            }
        }

        return null;
    }

    public function deleteGame(int $id) : bool
    {
        try {
            $success = $this->gameRepo->deleteGame($id);

            if ($success) {
                $this->cache->delete(self::KEY); // clear cache
            }

            return $success;
        } catch (\Throwable $e) {
            $this->logger->notice('Exception when calling ' . __METHOD__);

            return false;
        }
    }

    public function addGame(string $title, int $sysId, int $year, bool $completed) : int
    {
        try {
            $id = $this->gameRepo->addGame($title, $sysId, $year, $completed);

            if ($id) {
                $this->cache->delete(self::KEY); // clear cache
            }

            return $id;
        } catch (\Throwable $e) {
            $this->logger->notice('Exception when calling ' . __METHOD__);

            return 0;
        }
    }

    public function editGame(int $id, string $title, int $sysId, int $year, bool $completed) : bool
    {
        try {
            $success = $this->gameRepo->editGame($id, $title, $sysId, $year, $completed);

            if ($success) {
                $this->cache->delete(self::KEY); // clear cache
            }

            return $success;
        } catch (\Throwable $e) {
            $this->logger->notice('Exception when calling ' . __METHOD__);

            return false;
        }
    }

    private function getGamesFromCache() : array
    {
        // check for item in cache
        $games = $this->cache->get(self::KEY);

        if (!$games) {
            // not in cache, so make db call and put in cache
            $games = $this->gameRepo->getGames();
            $this->cache->add(self::KEY, $games);
        }

        return $games;
    }
}
